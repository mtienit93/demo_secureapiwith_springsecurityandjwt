package com.nmtien.controller;

import com.nmtien.entity.User;
import com.nmtien.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TestController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users")
    List<User> getAllUser(){
        return userRepository.findAll();
    }
}
