package com.nmtien.data;

import com.nmtien.entity.User;
import com.nmtien.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        if( userRepository.findByUserName("nmtien") == null){
            User user = new User();
            user.setUserName("nmtien");
            user.setPassword(passwordEncoder.encode("123"));
            userRepository.save(user);
        }
    }
}
