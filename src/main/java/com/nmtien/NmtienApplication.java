package com.nmtien;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NmtienApplication {

	public static void main(String[] args) {
		SpringApplication.run(NmtienApplication.class, args);
	}
}
